Projeto da API de fornecimento dos dados dos usuários e rotas para serem mostrados no aplicativo RTRK

Passos para a implementação:

- [x] Criar função de conversão dos arquivos GPX para JSON
- [x] Adicionar express
- [x] Adicionar MongoDB
- [x] Criar conexão com o banco de dados
- [x] Criar modelo de Schema das rotas
- [x] Criar modelo de Schema dos usuários
- [x] Criar Docker File da aplicação
- [x] Criar Docker-Compose yml para subir o serviço do DB
- [x] Criar página cliente para upload do arquivo GPX
- [x] Subir rota com express da página cliente para fazer upload do arquivo GPX
- [x] Adicionar dependencias do graphQL
- [x] Criar tipos User e Routes no graphQL
- [x] Criar resolvers de CREATE de Rotas
- [x] Criar resolvers de READ de Rotas
- [ ] Criar resolvers de UPDATE de Rotas
- [ ] Criar resolvers de DELETE de Rotas
- [x] Criar resolvers de CREATE de Usuários
- [x] Criar resolvers de READ de Usuários
- [ ] Criar resolvers de UPDATE de Usuários
- [x] Criar resolvers de DELETE de Usuários
- [x] Criar método de getRoutesByUserID
- [x] Criar método de getTracksByRoutesID
- [ ] Criar conta no mLab para hospedagem do banco
- [ ] Setar variárveis de ambiente para deploy (API_URL/MONGO_URL)
- [ ] Criar Procfile para deploy do Heroku
- [ ] Criar conta para o RTRK no Github
- [ ] Subir projeto como privado na conta RTRK do Github
- [ ] Deploy da Aplicação no Heroku
