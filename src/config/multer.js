const multer = require("multer");
const path = require("path");
const crypto = require("crypto");

module.exports = {
	dest: path.resolve(__dirname, "..", "tmp", "uploads"),
	storage: multer.diskStorage({
		destination: (req, file, callback) => {
			callback(null, path.resolve(__dirname, "..", "..", "tmp", "uploads"));
		},
		filename: (req, file, callback) => {
			crypto.randomBytes(4, (err, hash) => {
				if (err) callback(err);
				// const originalName = file.originalname.split("-")[1];
				const fileName = `${hash.toString("hex")}-${file.originalname}`;
				callback(null, fileName);
			});
		},
	}),
};
