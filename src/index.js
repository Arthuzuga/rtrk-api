const express = require("express");
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const graphqlHTTP = require("express-graphql");
const schema = require("../src/schema/schema");

const convertGpxToJson = require("./actions/converter");

const multer = require("multer");
const multerConfig = require("./config/multer");

const Tracks = require("./models/Tracks");
const Routes = require("./models/Routes");

const fs = require("fs");

app.use(cors({ origin: "*" }));
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");

	res.header(
		"Access-Control-Allow-Methods",
		"GET, POST, OPTIONS, PUT, PATCH, DELETE",
	);

	res.header("Access-Control-Allow-Headers", "X-Requested-With,content-type");

	res.setHeader("Access-Control-Allow-Credentials", false);

	next();
});

app.set("view engine", "ejs");

try {
	mongoose.connect(`mongodb://mongo:27017/upload`, {
		// mongoose.connect(`mongodb://localhost:27017/upload`, {
		useNewUrlParser: true,
	});
	mongoose.connection.once("open", () => {
		console.log("Connected to database");
	});
} catch (error) {
	throw new Error(error.message);
}

app.get("/", (req, res) => res.render("home"));
app.post("/", multer(multerConfig).single("img"), async (req, res) => {
	const path = req.file.path;
	const fileName = req.file.originalname.split(".")[0];

	convertGpxToJson(path, fileName);

	const { price, originPort, name, shipType } = req.body;

	const route = Routes.findOne({ name });

	if (!!route) {
		Routes.create({
			name,
			price,
			originPort,
			shipType,
		});
	}

	res.send("ok");
	try {
		fs.unlinkSync(path);
	} catch (error) {
		console.log("Nao consegui excluir");
	}
});
// app.get("/tracks", async(req, res) => {

// });
app.delete("/", async (req, res) => {
	await Tracks.deleteMany({});
	res.send("Dados apagados com sucesso");
});

app.use(
	"/graphql",
	graphqlHTTP({
		schema,
		graphiql: true,
	}),
);

app.listen(3000, () => console.log("Server is ALIVE!!!"));
