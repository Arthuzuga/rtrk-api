"use strict";
const gpxParse = require("gpx-parse");

const Tracks = require("../models/Tracks");

const convertGpxToJson = (path, fileName) => {
	let tracksSaved = [];
	gpxParse.parseGpxFromFile(path, async function(_, data) {
		const tracks = data.tracks;
		tracksSaved = tracks.map((track) => ({
			id: Date.now(),
			name: track.name,
			segments: JSON.stringify(track.segments[0]),
			length: track.length(),
		}));
		tracks.forEach((track) => {
			Tracks.create({
				id: track.id,
				name: track.name,
				segments: JSON.stringify(track.segments[0]),
				routeName: fileName,
			});
		});
		const files = await Tracks.find({});
		console.log(files);
	});

	return tracksSaved;
};

module.exports = convertGpxToJson;
