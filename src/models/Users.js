const mongoose = require("mongoose");

const UsersSchema = new mongoose.Schema({
	name: String,
	phone: String,
	email: String,
	password: String,
	routesIds: [String],
});

module.exports = mongoose.model("Users", UsersSchema);
