const mongoose = require("mongoose");

const TracksSchema = new mongoose.Schema({
	id: String,
	name: String,
	segments: String,
	routeId: String,
});

module.exports = mongoose.model("Tracks", TracksSchema);
