const mongoose = require("mongoose");

const RoutesSchema = new mongoose.Schema({
	name: String,
	price: Number,
	originPort: String,
	shipType: String,
});

module.exports = mongoose.model("Routes", RoutesSchema);
