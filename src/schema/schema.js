const graphql = require("graphql");
const Tracks = require("../models/Tracks");
const Routes = require("../models/Routes");
const Users = require("../models/Users");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const {
	GraphQLObjectType,
	GraphQLID,
	GraphQLString,
	GraphQLNonNull,
	GraphQLList,
	GraphQLSchema,
	GraphQLInt,
} = graphql;

const TrackType = new GraphQLObjectType({
	name: "Track",
	fields: () => ({
		id: {
			type: GraphQLID,
		},
		name: {
			type: GraphQLString,
		},
		segments: {
			type: GraphQLString,
		},
		route: {
			type: RouteType,
			resolve(parentValue, args) {
				return Routes.findById(parentValue.routeId);
			},
		},
	}),
});

const RouteType = new GraphQLObjectType({
	name: "Route",
	fields: () => ({
		id: {
			type: GraphQLID,
		},
		name: {
			type: GraphQLString,
		},
		price: {
			type: GraphQLInt,
		},
		tracks: {
			type: new GraphQLList(TrackType),
			resolve(parentValue, args) {
				return Tracks.find({
					routeId: parentValue.id,
				});
			},
		},
	}),
});

const UserType = new GraphQLObjectType({
	name: "User",
	fields: () => ({
		id: {
			type: GraphQLID,
		},
		name: {
			type: GraphQLString,
		},
		phone: {
			type: GraphQLString,
		},
		email: {
			type: GraphQLString,
		},
		password: {
			type: GraphQLString,
		},
		routesId: {
			type: new GraphQLInt(GraphQLString),
		},
	}),
});

const RootQuery = new GraphQLObjectType({
	name: "RootQueryType",
	fields: {
		track: {
			type: TrackType,
			args: {
				id: {
					type: GraphQLID,
				},
			},
			resolve(parentValue, args) {
				return Tracks.findById(args.id);
			},
		},
		tracks: {
			type: new GraphQLList(TrackType),
			resolve(parentValue, args) {
				return Tracks.find({});
			},
		},
		route: {
			type: RouteType,
			args: {
				id: {
					type: GraphQLID,
				},
			},
			resolve(parentValue, args) {
				return Routes.findById(args.id);
			},
		},
		routes: {
			type: new GraphQLList(RouteType),
			resolve(parentValue, args) {
				return Routes.find({});
			},
		},
		getRoutesByUserId: {
			type: new GraphQLList(RouteType),
			args: {
				routesIds: {
					type: [GraphQLString],
				},
			},
			resolve(parentValue, args) {
				const routesIds = args.routesIds;

				let routes = routesIds.map(async (id) => {
					route = await Routes.findById(id);
					return [...routes, route];
				});

				return routes;
			},
		},
		user: {
			type: UserType,
			args: {
				email: {
					type: new GraphQLNonNull(GraphQLString),
				},
				password: {
					type: new GraphQLNonNull(GraphQLString),
				},
			},
			resolve(parentValue, args) {
				/* 
                    login method will be here
                */
				const user = Users.find({ email: args.email });
				if (!user) {
					throw new Error("User doesn't exists");
				}
				const valid = bcrypt.compare(args.password, user.password);

				if (!valid) {
					throw new Error("Invalid Password");
				}

				return user;
				// return Users.findById(args.id);
			},
		},
		users: {
			type: new GraphQLList(UserType),
			resolve(parentValue, args) {
				return Users.find({});
			},
		},
	},
});

const mutation = new GraphQLObjectType({
	name: "Mutation",
	fields: {
		createUser: {
			type: UserType,
			args: {
				name: {
					type: new GraphQLNonNull(GraphQLString),
				},
				phone: {
					type: new GraphQLNonNull(GraphQLString),
				},
				email: {
					type: new GraphQLNonNull(GraphQLString),
				},
				password: {
					type: new GraphQLNonNull(GraphQLString),
				},
				routesIds: {
					type: [GraphQLString],
				},
			},
			resolve(parentValue, args) {
				let user = new Users({
					name: args.name,
					phone: args.phone,
					email: args.email,
					password: bcrypt.hash(args.password, 10),
					routesIds: args.routesIds,
				});
				return user.save();
			},
		},
		deleteUser: {
			type: UserType,
			args: {
				id: {
					type: new GraphQLNonNull(GraphQLID),
				},
			},
			resolve(parentValue, args) {
				Users.deleteOne(Users.findById(args.id), (err, obj) => {
					if (err) throw err;
					console.log("User deleted, id: ", args.id);
				});
				return Users.find({});
			},
		},
		// updateUser: {},
		// resetPassword: {},
		addRouteToAnUser: {
			type: UserType,
			args: {
				id: {
					type: new GraphQLNonNull(GraphQLID),
				},
				routesIds: {
					type: [GraphQLString],
				},
			},
			resolve(parentValue, args) {
				const user = Users.findById(args.id);
				const newRoutesArray = [...user.routesIds, ...args.routesIds];
				Users.updateOne(
					user,
					{
						$set: {
							routesIds: newRoutesArray,
						},
					},
					(err, obj) => {
						if (err) throw err;
						console.log("Route added in user's info");
					},
				);
				return user;
			},
		},
	},
});

module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation,
});
